/*
 * Created from 'scheme.tl' by 'mtprotoc'
 *
 * Copyright (c) 2021-present,  Teamgram Studio (https://teamgram.io).
 *  All rights reserved.
 *
 * Author: teamgramio (teamgram.io@gmail.com)
 */

package config

import (
	"github.com/teamgram/teamgram-server/pkg/net2"
	"github.com/zeromicro/go-zero/zrpc"
)

type Config struct {
	zrpc.RpcServerConf
	MaxProc        int
	KeyData        string
	KeyFingerprint string
	Server         *net2.TcpServerConfig
	WsServer       *net2.WebsocketConfig
	Session        zrpc.RpcClientConf
}

//type TcpServerConfig struct {
//	Addrs      []string
//	Multicore  bool
//	SendBuf    int
//	ReceiveBuf int
//}
