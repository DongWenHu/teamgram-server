// Copyright (c) 2020-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package langpack

import (
	"context"
	"github.com/teamgram/proto/mtproto"
	"github.com/teamgram/proto/mtproto/rpc/metadata"
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/model"
	log "github.com/zeromicro/go-zero/core/logx"
)

// LangpackGetLanguages
// @Description: 获取指定平台的所有语言包
// @receiver service
// @param ctx
// @param request
// @return *mtproto.Vector_LangPackLanguage
// @return error
func (service *Service) LangpackGetLanguages(ctx context.Context, request *mtproto.TLLangpackGetLanguages) (*mtproto.Vector_LangPackLanguage, error) {
	md := metadata.RpcMetadataFromIncoming(ctx)
	log.Debugf("langpack.getLanguages - metadata: %s, request: %s", md.DebugString(), request.DebugString())

	// 400	BOT_METHOD_INVALID	This method can't be used by a bot
	if md.IsBot {
		err := mtproto.ErrBotMethodInvalid
		log.Errorf("langpack.getLanguages - error: %v", err)
		return nil, err
	}

	langPack := request.LangPack
	if langPack == "" {
		langPack = md.Langpack
	}

	// 400	LANG_PACK_INVALID	The provided language pack is invalid
	if !model.CheckLangPackInvalid(langPack) {
		err := mtproto.ErrLangPackInvalid
		log.Errorf("langpack.getLanguages - error: %v", err)
		return nil, err
	}

	languages := new(mtproto.Vector_LangPackLanguage)
	languages.Datas = service.GetLanguages(ctx, langPack)

	log.Debugf("langpack.getLanguages - reply: %s", languages.DebugString())
	return languages, nil
}
