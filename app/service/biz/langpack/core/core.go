// Copyright (c) 2019-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package core

import (
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/dao"
)

type LangPackCore struct {
	*dao.Dao
}

func New(dao *dao.Dao) *LangPackCore {
	return &LangPackCore{dao}
}
