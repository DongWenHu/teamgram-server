// Copyright (c) 2020-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package langpack

import (
	"context"
	"github.com/teamgram/proto/mtproto"
	"github.com/teamgram/proto/mtproto/rpc/metadata"
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/model"
	log "github.com/zeromicro/go-zero/core/logx"
)

// LangpackGetLangPack
// @Description: 获取指定类型的语言包
// @receiver service
// @param ctx
// @param request
// @return *mtproto.LangPackDifference
// @return error
func (service *Service) LangpackGetLangPack(ctx context.Context, request *mtproto.TLLangpackGetLangPack) (*mtproto.LangPackDifference, error) {
	md := metadata.RpcMetadataFromIncoming(ctx)
	log.Debugf("langpack.getLangPack - metadata: %s, request: %s", md.DebugString(), request.DebugString())

	// 400	BOT_METHOD_INVALID	This method can't be used by a bot
	if md.IsBot {
		err := mtproto.ErrBotMethodInvalid
		log.Errorf("langpack.getLangPack - error: %v", err)
		return nil, err
	}

	langPack := request.LangPack
	if langPack == "" {
		langPack = md.Langpack
	}

	// 400	LANG_PACK_INVALID	The provided language pack is invalid
	if !model.CheckLangPackInvalid(langPack) {
		err := mtproto.ErrLangPackInvalid
		log.Errorf("langpack.getLangPack - error: %v", err)
		return nil, err
	}

	// check
	version, _, err := service.GetLanguage(ctx, langPack, request.LangCode)
	if err != nil {
		log.Errorf("langpack.getLangPack - error: %v", err)
		err = mtproto.ErrLangCodeNotSupported
		return nil, err
	}

	diff := mtproto.MakeTLLangPackDifference(&mtproto.LangPackDifference{
		LangCode:    request.LangCode,
		FromVersion: 0,
		Version:     version,
		Strings:     service.GetStrings(ctx, langPack, request.LangCode),
	}).To_LangPackDifference()

	log.Debugf("langpack.getLangPack#f2f2330a - reply: %s", diff.DebugString())
	return diff, nil
}
