// Copyright (c) 2020-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package langpack

import (
	"context"
	"github.com/teamgram/proto/mtproto"
	"github.com/teamgram/proto/mtproto/rpc/metadata"
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/model"
	log "github.com/zeromicro/go-zero/core/logx"
)

// LangpackGetDifference
// @Description: 获取指定类型的语言包差异
// @receiver service
// @param ctx
// @param request
// @return *mtproto.LangPackDifference
// @return error
func (service *Service) LangpackGetDifference(ctx context.Context, request *mtproto.TLLangpackGetDifference) (*mtproto.LangPackDifference, error) {
	md := metadata.RpcMetadataFromIncoming(ctx)
	log.Debugf("langpack.getDifference - metadata: %s, request: %s", md.DebugString(), request.DebugString())

	// 400	BOT_METHOD_INVALID	This method can't be used by a bot
	if md.IsBot {
		err := mtproto.ErrBotMethodInvalid
		log.Errorf("langpack.getDifference - error: %v", err)
		return nil, err
	}

	langPack := request.LangPack
	if langPack == "" {
		langPack = md.Langpack
	}

	// 400	LANG_PACK_INVALID	The provided language pack is invalid
	if !model.CheckLangPackInvalid(langPack) {
		err := mtproto.ErrLangPackInvalid
		log.Errorf("langpack.getLanguages - error: %v", err)
		return nil, err
	}

	// check
	version, _, err := service.GetLanguage(ctx, langPack, request.LangCode)
	if err != nil {
		log.Errorf("langpack.getDifference - error: %v", err)
		err = mtproto.ErrLangCodeNotSupported
		return nil, err
	}

	diff := mtproto.MakeTLLangPackDifference(&mtproto.LangPackDifference{
		LangCode:    request.LangCode,
		FromVersion: request.FromVersion,
		Version:     version,
		Strings:     nil,
	}).To_LangPackDifference()

	if request.FromVersion >= version {
		diff.Strings = []*mtproto.LangPackString{}
	} else {
		diff.Strings = service.GetDifference(ctx, langPack, request.LangCode, request.FromVersion)
	}

	log.Debugf("langpack.getDifference - reply: %s", diff.DebugString())
	return diff, nil
}
