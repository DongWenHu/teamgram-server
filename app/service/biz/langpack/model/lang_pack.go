// Copyright (c) 2019-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package model

import (
	"github.com/teamgram/proto/mtproto"
)

type LangPacks struct {
	LangCode          string                    `json:"lang_code,omitempty"`
	Version           int32                     `json:"version,omitempty"`
	Strings           []*mtproto.LangPackString `json:"strings,omitempty"`
	StringPluralizeds []*mtproto.LangPackString `json:"string_pluralizeds,omitempty"`
	StringDeleteds    []*mtproto.LangPackString `json:"string_deleteds,omitempty"`
}

func (m *LangPacks) Query(k string) string {
	for _, lp := range m.Strings {
		if lp.GetKey() == k {
			return lp.GetValue()
		}
	}
	return ""
}
