// Copyright (c) 2019-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package langpack

import "github.com/teamgram/teamgram-server/app/service/biz/langpack/config"

type (
	Config = config.Config
)

func New(c Config) *Service {
	return Init(c)
}
