/*
 * WARNING! All changes made in this file will be lost!
 *   Created from by 'nebula-dal-generator'
 *
 * Copyright (c) 2020-present,  NebulaChat Studio (https://nebula.chat).
 *  All rights reserved.
 *
 * Author: Benqi (wubenqi@gmail.com)
 */

package mysql_dao

import (
	"context"
	"database/sql"
	"github.com/teamgram/marmota/pkg/stores/sqlx"
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/dal/dataobject"
	log "github.com/zeromicro/go-zero/core/logx"
)

type AppLanguagesDAO struct {
	db *sqlx.DB
}

func NewAppLanguagesDAO(db *sqlx.DB) *AppLanguagesDAO {
	return &AppLanguagesDAO{db}
}

// insert into app_languages(app, lang_code, version, strings_count, translated_count, state) values (:app, :lang_code, :version, :strings_count, :translated_count, 0) on duplicate key update version = values(version), strings_count = values(strings_count), translated_count = values(translated_count), state = 0
// TODO(@benqi): sqlmap
func (appLanguagesDAO *AppLanguagesDAO) InsertOrUpdate(ctx context.Context, do *dataobject.AppLanguagesDO) (lastInsertId, rowsAffected int64, err error) {
	var (
		query = "insert into app_languages(app, lang_code, version, strings_count, translated_count, state) values (:app, :lang_code, :version, :strings_count, :translated_count, 0) on duplicate key update version = values(version), strings_count = values(strings_count), translated_count = values(translated_count), state = 0"
		r     sql.Result
	)

	r, err = appLanguagesDAO.db.NamedExec(ctx, query, do)
	if err != nil {
		log.Errorf("namedExec in InsertOrUpdate(%v), error: %v", do, err)
		return
	}

	lastInsertId, err = r.LastInsertId()
	if err != nil {
		log.Errorf("lastInsertId in InsertOrUpdate(%v)_error: %v", do, err)
		return
	}
	rowsAffected, err = r.RowsAffected()
	if err != nil {
		log.Errorf("rowsAffected in InsertOrUpdate(%v)_error: %v", do, err)
	}

	return
}

// insert into app_languages(app, lang_code, version, strings_count, translated_count, state) values (:app, :lang_code, :version, :strings_count, :translated_count, 0) on duplicate key update version = values(version), strings_count = values(strings_count), translated_count = values(translated_count), state = 0
// TODO(@benqi): sqlmap
func (appLanguagesDAO *AppLanguagesDAO) InsertOrUpdateTx(tx *sqlx.Tx, do *dataobject.AppLanguagesDO) (lastInsertId, rowsAffected int64, err error) {
	var (
		query = "insert into app_languages(app, lang_code, version, strings_count, translated_count, state) values (:app, :lang_code, :version, :strings_count, :translated_count, 0) on duplicate key update version = values(version), strings_count = values(strings_count), translated_count = values(translated_count), state = 0"
		r     sql.Result
	)

	r, err = tx.NamedExec(query, do)
	if err != nil {
		log.Errorf("namedExec in InsertOrUpdate(%v), error: %v", do, err)
		return
	}

	lastInsertId, err = r.LastInsertId()
	if err != nil {
		log.Errorf("lastInsertId in InsertOrUpdate(%v)_error: %v", do, err)
		return
	}
	rowsAffected, err = r.RowsAffected()
	if err != nil {
		log.Errorf("rowsAffected in InsertOrUpdate(%v)_error: %v", do, err)
	}

	return
}

// select id, app, lang_code, version, strings_count, translated_count from app_languages where app = :app and state = 0
// TODO(@benqi): sqlmap
func (appLanguagesDAO *AppLanguagesDAO) SelectLanguageList(ctx context.Context, app string) (rList []dataobject.AppLanguagesDO, err error) {
	var (
		query = "select id, app, lang_code, version, strings_count, translated_count from app_languages where app = ? and state = 0"
	    values []dataobject.AppLanguagesDO
	)
	err = appLanguagesDAO.db.QueryRowsPartial(ctx, &rList, query, app)

	if err != nil {
		log.Errorf("queryx in SelectLanguageList(_), error: %v", err)
		return
	}

	rList = values

	return
}

// select id, app, lang_code, version, strings_count, translated_count from app_languages where app = :app and lang_code = :lang_code and state = 0
// TODO(@benqi): sqlmap
func (appLanguagesDAO *AppLanguagesDAO) SelectLanguage(ctx context.Context, app string, lang_code string) (rValue *dataobject.AppLanguagesDO, err error) {
	var (
		query = "select id, app, lang_code, version, strings_count, translated_count from app_languages where app = ? and lang_code = ? and state = 0"
		do  = &dataobject.AppLanguagesDO{}
	)
	err = appLanguagesDAO.db.QueryRowPartial(ctx, do, query, app, lang_code)

	if err != nil {
		log.Errorf("queryx in SelectLanguage(_), error: %v", err)
		return
	}
	rValue = do

	return
}
