/*
 * WARNING! All changes made in this file will be lost!
 *   Created from by 'nebula-dal-generator'
 *
 * Copyright (c) 2020-present,  NebulaChat Studio (https://nebula.chat).
 *  All rights reserved.
 *
 * Author: Benqi (wubenqi@gmail.com)
 */

package mysql_dao

import (
	"context"
	"github.com/teamgram/marmota/pkg/stores/sqlx"
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/dal/dataobject"
	log "github.com/zeromicro/go-zero/core/logx"
)

type LangPackStringsDAO struct {
	db *sqlx.DB
}

func NewLangPackStringsDAO(db *sqlx.DB) *LangPackStringsDAO {
	return &LangPackStringsDAO{db}
}

// select lang_pack, lang_code, version, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value from lang_pack_strings where lang_pack = :lang_pack and lang_code = :lang_code and key2 in (:keyList) and deleted = 0
// TODO(@benqi): sqlmap
func (langPackStringsDAO *LangPackStringsDAO) SelectListByKeyList(ctx context.Context, lang_pack string, lang_code string, keyList []string) (rList []dataobject.LangPackStringsDO, err error) {
	var (
		query = "select lang_pack, lang_code, version, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value from lang_pack_strings where lang_pack = ? and lang_code = ? and key2 in (?) and deleted = 0"
		a     []interface{}
	)
	query, a, err = sqlx.In(query, lang_pack, lang_code, keyList)
	if err != nil {
		// r sql.Result
		log.Errorf("sqlx.In in SelectListByKeyList(_), error: %v", err)
		return
	}
	// sqlx.In returns queries with the `?` bindvar, we can rebind it for our backend
	// mysql ignore
	// query = langPackStringsDAO.db.Rebind(query)
	err = langPackStringsDAO.db.QueryRowsPartial(ctx, &rList, query, a...)

	if err != nil {
		log.Errorf("queryx in SelectListByKeyList(_), error: %v", err)
		return
	}

	return
}

// select lang_pack, lang_code, version, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value from lang_pack_strings where lang_pack = :lang_pack and lang_code = :lang_code and deleted = 0
// TODO(@benqi): sqlmap
func (langPackStringsDAO *LangPackStringsDAO) SelectList(ctx context.Context, lang_pack string, lang_code string) (rList []dataobject.LangPackStringsDO, err error) {
	var (
		query = "select lang_pack, lang_code, version, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value from lang_pack_strings where lang_pack = ? and lang_code = ? and deleted = 0"
	)
	err = langPackStringsDAO.db.QueryRowsPartial(ctx, &rList, query, lang_pack, lang_code)

	if err != nil {
		log.Errorf("queryx in SelectList(_), error: %v", err)
		return
	}

	return
}

// select lang_pack, lang_code, version, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value from lang_pack_strings where lang_pack = :lang_pack and lang_code = :lang_code and version > :version and deleted = 0
// TODO(@benqi): sqlmap
func (langPackStringsDAO *LangPackStringsDAO) SelectGTVersionList(ctx context.Context, lang_pack string, lang_code string, version int32) (rList []dataobject.LangPackStringsDO, err error) {
	var (
		query = "select lang_pack, lang_code, version, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value from lang_pack_strings where lang_pack = ? and lang_code = ? and version > ? and deleted = 0"
	)
	err = langPackStringsDAO.db.QueryRowsPartial(ctx, &rList, query, lang_pack, lang_code, version)

	if err != nil {
		log.Errorf("queryx in SelectGTVersionList(_), error: %v", err)
		return
	}

	return
}
