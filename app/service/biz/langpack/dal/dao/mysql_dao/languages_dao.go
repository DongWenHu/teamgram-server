/*
 * WARNING! All changes made in this file will be lost!
 *   Created from by 'nebula-dal-generator'
 *
 * Copyright (c) 2020-present,  NebulaChat Studio (https://nebula.chat).
 *  All rights reserved.
 *
 * Author: Benqi (wubenqi@gmail.com)
 */

package mysql_dao

import (
	"context"
	"github.com/teamgram/marmota/pkg/stores/sqlx"
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/dal/dataobject"
	log "github.com/zeromicro/go-zero/core/logx"
)

type LanguagesDAO struct {
	db *sqlx.DB
}

func NewLanguagesDAO(db *sqlx.DB) *LanguagesDAO {
	return &LanguagesDAO{db}
}

// select lang_code from languages
// TODO(@benqi): sqlmap
func (dao *LanguagesDAO) SelectAllLangCode(ctx context.Context) (rList []string, err error) {
	var query = "select lang_code from languages"
	err = dao.db.QueryRowsPartial(ctx, &rList, query)

	if err != nil {
		log.Errorf("select in SelectAllLangCode(_), error: %v", err)
	}

	return
}

// select id, lang_code, base_lang_code, link, official, rtl, beta, name, native_name, plural_code, translations_url from languages where state = 0 and lang_code in (:codeList)
// TODO(@benqi): sqlmap
func (dao *LanguagesDAO) SelectLanguageList(ctx context.Context, codeList []string) (rList []dataobject.LanguagesDO, err error) {
	var (
		query = "select id, lang_code, base_lang_code, link, official, rtl, beta, name, native_name, plural_code, translations_url from languages where state = 0 and lang_code in (?) order by id desc"
		a     []interface{}
	)
	query, a, err = sqlx.In(query, codeList)
	if err != nil {
		// r sql.Result
		log.Errorf("sqlx.In in SelectLanguageList(_), error: %v", err)
		return
	}
	// sqlx.In returns queries with the `?` bindvar, we can rebind it for our backend
	// mysql ignore
	// query = dao.db.Rebind(query)
	err = dao.db.QueryRowsPartial(ctx, &rList, query, a...)

	if err != nil {
		log.Errorf("queryx in SelectLanguageList(_), error: %v", err)
		return
	}

	return
}

// select id, lang_code, base_lang_code, link, official, rtl, beta, name, native_name, plural_code, translations_url from languages where lang_code = :lang_code
// TODO(@benqi): sqlmap
func (dao *LanguagesDAO) SelectLanguage(ctx context.Context, lang_code string) (rValue *dataobject.LanguagesDO, err error) {
	var (
		query = "select id, lang_code, base_lang_code, link, official, rtl, beta, name, native_name, plural_code, translations_url from languages where lang_code = ?"
		do dataobject.LanguagesDO
	)
	err = dao.db.QueryRowPartial(ctx, &do, query, lang_code)

	if err != nil {
		log.Errorf("queryx in SelectLanguage(_), error: %v", err)
		return
	}
	rValue = &do
	return
}
