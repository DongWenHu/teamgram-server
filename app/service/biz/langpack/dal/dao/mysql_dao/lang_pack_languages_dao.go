/*
 * WARNING! All changes made in this file will be lost!
 *   Created from by 'nebula-dal-generator'
 *
 * Copyright (c) 2020-present,  NebulaChat Studio (https://nebula.chat).
 *  All rights reserved.
 *
 * Author: Benqi (wubenqi@gmail.com)
 */

package mysql_dao

import (
	"context"
	"github.com/teamgram/marmota/pkg/stores/sqlx"
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/dal/dataobject"
	log "github.com/zeromicro/go-zero/core/logx"
)

type LangPackLanguagesDAO struct {
	db *sqlx.DB
}

func NewLangPackLanguagesDAO(db *sqlx.DB) *LangPackLanguagesDAO {
	return &LangPackLanguagesDAO{db}
}

// select lang_pack, lang_code, version, base_lang_code, official, rtl, beta, name, native_name, plural_code, strings_count, translated_count, translations_url from lang_pack_languages where lang_pack = :lang_pack and state = 0
// TODO(@benqi): sqlmap
func (appLanguagesDAO *LangPackLanguagesDAO) SelectByLangPack(ctx context.Context, lang_pack string) (rList []dataobject.LangPackLanguagesDO, err error) {
	var (
		query = "select lang_pack, lang_code, version, base_lang_code, official, rtl, beta, name, native_name, plural_code, strings_count, translated_count, translations_url from lang_pack_languages where lang_pack = ? and state = 0"
	)
	err = appLanguagesDAO.db.QueryRowsPartial(ctx, &rList, query, lang_pack)

	if err != nil {
		log.Errorf("queryx in SelectByLangPack(_), error: %v", err)
		return
	}

	return
}

// select lang_pack, lang_code, version, base_lang_code, official, rtl, beta, name, native_name, plural_code, strings_count, translated_count, translations_url from lang_pack_languages where lang_pack = :lang_pack and lang_code = :lang_code and state = 0
// TODO(@benqi): sqlmap
func (appLanguagesDAO *LangPackLanguagesDAO) SelectByLangPackCode(ctx context.Context, lang_pack string, lang_code string) (rValue *dataobject.LangPackLanguagesDO, err error) {
	var (
		query = "select lang_pack, lang_code, version, base_lang_code, official, rtl, beta, name, native_name, plural_code, strings_count, translated_count, translations_url from lang_pack_languages where lang_pack = ? and lang_code = ? and state = 0"
		do dataobject.LangPackLanguagesDO
	)
	err = appLanguagesDAO.db.QueryRowPartial(ctx, &do, query, lang_pack, lang_code)

	if err != nil {
		log.Errorf("queryx in SelectByLangPackCode(_), error: %v", err)
		return
	}
	rValue = &do
	return
}

// select version from lang_pack_languages where lang_pack = :lang_pack and lang_code = :lang_code and state = 0
// TODO(@benqi): sqlmap
func (appLanguagesDAO *LangPackLanguagesDAO) SelectVersion(ctx context.Context, lang_pack string, lang_code string) (rValue int32, err error) {
	var query = "select version from lang_pack_languages where lang_pack = ? and lang_code = ? and state = 0"
	err = appLanguagesDAO.db.QueryRowPartial(ctx, &rValue, query, lang_pack, lang_code)

	if err != nil {
		log.Errorf("get in SelectVersion(_), error: %v", err)
	}

	return
}
