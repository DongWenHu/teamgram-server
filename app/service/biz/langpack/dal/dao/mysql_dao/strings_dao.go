/*
 * WARNING! All changes made in this file will be lost!
 *   Created from by 'nebula-dal-generator'
 *
 * Copyright (c) 2020-present,  NebulaChat Studio (https://nebula.chat).
 *  All rights reserved.
 *
 * Author: Benqi (wubenqi@gmail.com)
 */

package mysql_dao

import (
	"context"
	"database/sql"
	"github.com/teamgram/marmota/pkg/stores/sqlx"
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/dal/dataobject"
	log "github.com/zeromicro/go-zero/core/logx"
)

type StringsDAO struct {
	db *sqlx.DB
}

func NewStringsDAO(db *sqlx.DB) *StringsDAO {
	return &StringsDAO{db}
}

// insert into strings(lang_pack, lang_code, version, key_index, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value) values (:lang_pack, :lang_code, :version, :key_index, :key2, :pluralized, :value, :zero_value, :one_value, :two_value, :few_value, :many_value, :other_value) on duplicate key update version = values(version), value = values(value), zero_value = values(zero_value), one_value = values(one_value), two_value = values(two_value), few_value = values(few_value), many_value = values(many_value), other_value = values(other_value)
// TODO(@benqi): sqlmap
func (stringsDAO *StringsDAO) InsertOrUpdate(ctx context.Context, do *dataobject.StringsDO) (lastInsertId, rowsAffected int64, err error) {
	var (
		query = "insert into strings(lang_pack, lang_code, version, key_index, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value) values (:lang_pack, :lang_code, :version, :key_index, :key2, :pluralized, :value, :zero_value, :one_value, :two_value, :few_value, :many_value, :other_value) on duplicate key update version = values(version), value = values(value), zero_value = values(zero_value), one_value = values(one_value), two_value = values(two_value), few_value = values(few_value), many_value = values(many_value), other_value = values(other_value)"
		r     sql.Result
	)

	r, err = stringsDAO.db.NamedExec(ctx, query, do)
	if err != nil {
		log.Errorf("namedExec in InsertOrUpdate(%v), error: %v", do, err)
		return
	}

	lastInsertId, err = r.LastInsertId()
	if err != nil {
		log.Errorf("lastInsertId in InsertOrUpdate(%v)_error: %v", do, err)
		return
	}
	rowsAffected, err = r.RowsAffected()
	if err != nil {
		log.Errorf("rowsAffected in InsertOrUpdate(%v)_error: %v", do, err)
	}

	return
}

// insert into strings(lang_pack, lang_code, version, key_index, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value) values (:lang_pack, :lang_code, :version, :key_index, :key2, :pluralized, :value, :zero_value, :one_value, :two_value, :few_value, :many_value, :other_value) on duplicate key update version = values(version), value = values(value), zero_value = values(zero_value), one_value = values(one_value), two_value = values(two_value), few_value = values(few_value), many_value = values(many_value), other_value = values(other_value)
// TODO(@benqi): sqlmap
func (stringsDAO *StringsDAO) InsertOrUpdateTx(tx *sqlx.Tx, do *dataobject.StringsDO) (lastInsertId, rowsAffected int64, err error) {
	var (
		query = "insert into strings(lang_pack, lang_code, version, key_index, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value) values (:lang_pack, :lang_code, :version, :key_index, :key2, :pluralized, :value, :zero_value, :one_value, :two_value, :few_value, :many_value, :other_value) on duplicate key update version = values(version), value = values(value), zero_value = values(zero_value), one_value = values(one_value), two_value = values(two_value), few_value = values(few_value), many_value = values(many_value), other_value = values(other_value)"
		r     sql.Result
	)

	r, err = tx.NamedExec(query, do)
	if err != nil {
		log.Errorf("namedExec in InsertOrUpdate(%v), error: %v", do, err)
		return
	}

	lastInsertId, err = r.LastInsertId()
	if err != nil {
		log.Errorf("lastInsertId in InsertOrUpdate(%v)_error: %v", do, err)
		return
	}
	rowsAffected, err = r.RowsAffected()
	if err != nil {
		log.Errorf("rowsAffected in InsertOrUpdate(%v)_error: %v", do, err)
	}

	return
}

// update strings set deleted = 1 where lang_pack = :lang_pack and lang_code = :lang_code and key2 = :key2
// TODO(@benqi): sqlmap
func (stringsDAO *StringsDAO) Delete(ctx context.Context, lang_pack string, lang_code string, key2 string) (rowsAffected int64, err error) {
	var (
		query   = "update strings set deleted = 1 where lang_pack = ? and lang_code = ? and key2 = ?"
		rResult sql.Result
	)
	rResult, err = stringsDAO.db.Exec(ctx, query, lang_pack, lang_code, key2)

	if err != nil {
		log.Errorf("exec in Delete(_), error: %v", err)
		return
	}

	rowsAffected, err = rResult.RowsAffected()
	if err != nil {
		log.Errorf("rowsAffected in Delete(_), error: %v", err)
	}

	return
}

// update strings set deleted = 1 where lang_pack = :lang_pack and lang_code = :lang_code and key2 = :key2
// TODO(@benqi): sqlmap
func (stringsDAO *StringsDAO) DeleteTx(tx *sqlx.Tx, lang_pack string, lang_code string, key2 string) (rowsAffected int64, err error) {
	var (
		query   = "update strings set deleted = 1 where lang_pack = ? and lang_code = ? and key2 = ?"
		rResult sql.Result
	)
	rResult, err = tx.Exec(query, lang_pack, lang_code, key2)

	if err != nil {
		log.Errorf("exec in Delete(_), error: %v", err)
		return
	}

	rowsAffected, err = rResult.RowsAffected()
	if err != nil {
		log.Errorf("rowsAffected in Delete(_), error: %v", err)
	}

	return
}

// select lang_pack, lang_code, version, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value from strings where lang_pack = :lang_pack and lang_code = :lang_code and key2 in (:keyList) and deleted = 0
// TODO(@benqi): sqlmap
func (stringsDAO *StringsDAO) SelectListByKeyList(ctx context.Context, lang_pack string, lang_code string, keyList []string) (rList []dataobject.StringsDO, err error) {
	var (
		query = "select lang_pack, lang_code, version, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value from strings where lang_pack = ? and lang_code = ? and key2 in (?) and deleted = 0"
		inter []interface{}
	)
	query, inter, err = sqlx.In(query, lang_pack, lang_code, keyList)
	if err != nil {
		// r sql.Result
		log.Errorf("sqlx.In in SelectListByKeyList(_), error: %v", err)
		return
	}
	// sqlx.In returns queries with the `?` bindvar, we can rebind it for our backend
	// mysql ignore
	// query = stringsDAO.db.Rebind(query)
	err = stringsDAO.db.QueryRowsPartial(ctx, &rList, query, inter...)

	if err != nil {
		log.Errorf("queryx in SelectListByKeyList(_), error: %v", err)
		return
	}

	return
}

// select lang_pack, lang_code, version, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value from strings where lang_pack = :lang_pack and lang_code = :lang_code and deleted = 0
// TODO(@benqi): sqlmap
func (stringsDAO *StringsDAO) SelectList(ctx context.Context, lang_pack string, lang_code string) (rList []dataobject.StringsDO, err error) {
	var (
		query = "select lang_pack, lang_code, version, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value from strings where lang_pack = ? and lang_code = ? and deleted = 0"
	)
	err = stringsDAO.db.QueryRowsPartial(ctx, &rList, query, lang_pack, lang_code)

	if err != nil {
		log.Errorf("queryx in SelectList(_), error: %v", err)
		return
	}

	return
}

// select lang_pack, lang_code, version, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value from strings where lang_pack = :lang_pack and lang_code = :lang_code and version > :version and deleted = 0
// TODO(@benqi): sqlmap
func (stringsDAO *StringsDAO) SelectGTVersionList(ctx context.Context, lang_pack string, lang_code string, version int32) (rList []dataobject.StringsDO, err error) {
	var (
		query = "select lang_pack, lang_code, version, key2, pluralized, value, zero_value, one_value, two_value, few_value, many_value, other_value from strings where lang_pack = ? and lang_code = ? and version > ? and deleted = 0"
	)
	err = stringsDAO.db.QueryRowsPartial(ctx, &rList, query, lang_pack, lang_code, version)

	if err != nil {
		log.Errorf("queryx in SelectGTVersionList(_), error: %v", err)
		return
	}

	return
}
