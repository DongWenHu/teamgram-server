/*
 * WARNING! All changes made in this file will be lost!
 *   Created from by 'nebula-dal-generator'
 *
 * Copyright (c) 2019-present,  NebulaChat Studio (https://nebula.chat).
 *  All rights reserved.
 *
 * Author: Benqi (wubenqi@gmail.com)
 */

package dataobject

// LangPackStringsDO
// @Description: 语言包补充值
type LangPackStringsDO struct {
	Id         int32  `json:"id" db:"id"`
	LangPack   string `json:"lang_pack" db:"lang_pack"`
	LangCode   string `json:"lang_code" db:"lang_code"`
	Version    int32  `json:"version" db:"version"`
	Key2       string `json:"key2" db:"key2"`
	Pluralized int8   `json:"pluralized" db:"pluralized"`
	Value      string `json:"value" db:"value"`
	ZeroValue  string `json:"zero_value" db:"zero_value"`
	OneValue   string `json:"one_value" db:"one_value"`
	TwoValue   string `json:"two_value" db:"two_value"`
	FewValue   string `json:"few_value" db:"few_value"`
	ManyValue  string `json:"many_value" db:"many_value"`
	OtherValue string `json:"other_value" db:"other_value"`
	Deleted    int8   `json:"deleted" db:"deleted"`
}
