/*
 * WARNING! All changes made in this file will be lost!
 *   Created from by 'nebula-dal-generator'
 *
 * Copyright (c) 2019-present,  NebulaChat Studio (https://nebula.chat).
 *  All rights reserved.
 *
 * Author: Benqi (wubenqi@gmail.com)
 */

package dataobject

// LangPackLanguagesDO
// @Description: 语言包类型
type LangPackLanguagesDO struct {
	Id              int32  `json:"id" db:"id"`
	LangPack        string `json:"lang_pack" db:"lang_pack"`
	LangCode        string `json:"lang_code" db:"lang_code"`
	Version         int32  `json:"version" db:"version"`
	BaseLangCode    string `json:"base_lang_code" db:"base_lang_code"`
	Official        int8   `json:"official" db:"official"`
	Rtl             int8   `json:"rtl" db:"rtl"`
	Beta            int8   `json:"beta" db:"beta"`
	Name            string `json:"name" db:"name"`
	NativeName      string `json:"native_name" db:"native_name"`
	PluralCode      string `json:"plural_code" db:"plural_code"`
	StringsCount    int32  `json:"strings_count" db:"strings_count"`
	TranslatedCount int32  `json:"translated_count" db:"translated_count"`
	TranslationsUrl string `json:"translations_url" db:"translations_url"`
	State           int8   `json:"state" db:"state"`
}
