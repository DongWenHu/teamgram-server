/*
 * WARNING! All changes made in this file will be lost!
 *   Created from by 'nebula-dal-generator'
 *
 * Copyright (c) 2019-present,  NebulaChat Studio (https://nebula.chat).
 *  All rights reserved.
 *
 * Author: Benqi (wubenqi@gmail.com)
 */

package dataobject

// LanguagesDO
// @Description: 语言包
type LanguagesDO struct {
	Id              int32  `json:"id" db:"id"`
	LangCode        string `json:"lang_code" db:"lang_code"`
	BaseLangCode    string `json:"base_lang_code" db:"base_lang_code"`
	Link            string `json:"link" db:"link"`
	Official        int8   `json:"official" db:"official"`
	Rtl             int8   `json:"rtl" db:"rtl"`
	Beta            int8   `json:"beta" db:"beta"`
	Name            string `json:"name" db:"name"`
	NativeName      string `json:"native_name" db:"native_name"`
	PluralCode      string `json:"plural_code" db:"plural_code"`
	TranslationsUrl string `json:"translations_url" db:"translations_url"`
	State           int8   `json:"state" db:"state"`
}
