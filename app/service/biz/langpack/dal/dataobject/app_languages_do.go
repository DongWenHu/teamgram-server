/*
 * WARNING! All changes made in this file will be lost!
 *   Created from by 'nebula-dal-generator'
 *
 * Copyright (c) 2019-present,  NebulaChat Studio (https://nebula.chat).
 *  All rights reserved.
 *
 * Author: Benqi (wubenqi@gmail.com)
 */

package dataobject

// AppLanguagesDO
// @Description: 语言类型
type AppLanguagesDO struct {
	Id              int32  `json:"id" db:"id"`
	App             string `json:"app" db:"app"`
	LangCode        string `json:"lang_code" db:"lang_code"`
	Version         int32  `json:"version" db:"version"`
	StringsCount    int32  `json:"strings_count" db:"strings_count"`
	TranslatedCount int32  `json:"translated_count" db:"translated_count"`
	State           int8   `json:"state" db:"state"`
}
