// Copyright (c) 2020-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package dao

import (
	"github.com/teamgram/marmota/pkg/stores/sqlx"
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/dal/dao/mysql_dao"
	"github.com/teamgram/teamgram-server/pkg/mysql_util"
)

type Mysql struct {
	*sqlx.DB
	*mysql_dao.LangPackStringsDAO
	*mysql_dao.LangPackLanguagesDAO
	*mysql_dao.LanguagesDAO
	*mysql_dao.AppLanguagesDAO
	*mysql_dao.StringsDAO
	*sqlx.CommonDAO
}

func newMysqlDao(mysql *sqlx.Config) *Mysql {
	db := mysql_util.GetSingletonSqlxDB(mysql)
	return &Mysql{
		DB:                   db,
		LangPackStringsDAO:   mysql_dao.NewLangPackStringsDAO(db),
		LangPackLanguagesDAO: mysql_dao.NewLangPackLanguagesDAO(db),
		LanguagesDAO:         mysql_dao.NewLanguagesDAO(db),
		AppLanguagesDAO:      mysql_dao.NewAppLanguagesDAO(db),
		StringsDAO:           mysql_dao.NewStringsDAO(db),
		CommonDAO:            sqlx.NewCommonDAO(db),
	}
}
