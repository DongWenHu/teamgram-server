// Copyright (c) 2020-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package dao

import (
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/config"
)

// Dao dao.
type Dao struct {
	*Mysql
}

// New new a dao and return.
func New(c config.Config) (dao *Dao) {
	dao = &Dao{
		Mysql: newMysqlDao(&c.Mysql),
	}

	return
}