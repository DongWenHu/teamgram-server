// Copyright (c) 2020-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package langpack

import (
	"context"
	"github.com/teamgram/proto/mtproto"
	"github.com/teamgram/proto/mtproto/rpc/metadata"
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/model"
	log "github.com/zeromicro/go-zero/core/logx"
)

// LangpackGetStrings
// @Description: 获取语言包指定key对应的国际化值
// @receiver service
// @param ctx
// @param request
// @return *mtproto.Vector_LangPackString
// @return error
func (service *Service) LangpackGetStrings(ctx context.Context, request *mtproto.TLLangpackGetStrings) (*mtproto.Vector_LangPackString, error) {
	md := metadata.RpcMetadataFromIncoming(ctx)
	log.Debugf("langpack.getStrings - metadata: %s, request: %s", md.DebugString(), request.DebugString())

	// 400	BOT_METHOD_INVALID	This method can't be used by a bot
	if md.IsBot {
		err := mtproto.ErrBotMethodInvalid
		log.Errorf("langpack.getStrings - error: %v", err)
		return nil, err
	}

	langPack := request.LangPack
	if langPack == "" {
		langPack = md.Langpack
	}

	// 400	LANG_PACK_INVALID	The provided language pack is invalid
	if !model.CheckLangPackInvalid(langPack) {
		err := mtproto.ErrLangPackInvalid
		log.Errorf("langpack.getStrings - error: %v", err)
		return nil, err
	}

	// check
	_, _, err := service.GetLanguage(ctx, langPack, request.LangCode)
	if err != nil {
		log.Errorf("langpack.getStrings - error: %v", err)
		err = mtproto.ErrLangCodeNotSupported
		return nil, err
	}

	langPackStrings := new(mtproto.Vector_LangPackString)
	langPackStrings.Datas = service.GetStringListByIdList(ctx, langPack, request.LangCode, request.Keys)

	log.Debugf("langpack.getStrings - reply: %s", langPackStrings.DebugString())
	return langPackStrings, nil
}
