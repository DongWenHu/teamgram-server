// Copyright (c) 2020-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package langpack

import (
	"context"
	"github.com/teamgram/proto/mtproto"
	"github.com/teamgram/proto/mtproto/rpc/metadata"
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/model"
	log "github.com/zeromicro/go-zero/core/logx"
)

/*
   TLRPC.TL_langpack_getLanguage req = new TLRPC.TL_langpack_getLanguage();
   req.lang_code = lang;
   req.lang_pack = "android";
   requestId[0] = ConnectionsManager.getInstance(currentAccount).sendRequest(req, (response, error) -> AndroidUtilities.runOnUIThread(() -> {
       try {
           progressDialog.dismiss();
       } catch (Exception e) {
           FileLog.e(e);
       }
       if (response instanceof TLRPC.TL_langPackLanguage) {
           TLRPC.TL_langPackLanguage res = (TLRPC.TL_langPackLanguage) response;
           showAlertDialog(AlertsCreator.createLanguageAlert(LaunchActivity.this, res));
       } else if (error != null) {
           if ("LANG_CODE_NOT_SUPPORTED".equals(error.text)) {
               showAlertDialog(AlertsCreator.createSimpleAlert(LaunchActivity.this, LocaleController.getString("LanguageUnsupportedError", R.string.LanguageUnsupportedError)));
           } else {
               showAlertDialog(AlertsCreator.createSimpleAlert(LaunchActivity.this, LocaleController.getString("ErrorOccurred", R.string.ErrorOccurred) + "\n" + error.text));
           }
       }
   }));
*/

// TODO(@benqi):
// langPackLanguage#eeca5ce3 flags:# official:flags.0?true rtl:flags.2?true beta:flags.3?true name:string native_name:string lang_code:string base_lang_code:flags.1?string plural_code:string strings_count:int translated_count:int translations_url:string = LangPackLanguage;

// LangpackGetLanguage
// @Description: 获取客户端语言种类
// @receiver service
// @param ctx
// @param request
// @return *mtproto.LangPackLanguage
// @return error
func (service *Service) LangpackGetLanguage(ctx context.Context, request *mtproto.TLLangpackGetLanguage) (*mtproto.LangPackLanguage, error) {
	md := metadata.RpcMetadataFromIncoming(ctx)
	log.Debugf("langpack.getLanguage - metadata: %s, request: %s", md.DebugString(), request.DebugString())

	// 400	BOT_METHOD_INVALID	This method can't be used by a bot
	if md.IsBot {
		err := mtproto.ErrBotMethodInvalid
		log.Errorf("langpack.getLanguage - error: %v", err)
		return nil, err
	}

	langPack := request.LangPack
	if langPack == "" {
		langPack = md.Langpack
	}

	// 400	LANG_PACK_INVALID	The provided language pack is invalid
	if !model.CheckLangPackInvalid(langPack) {
		err := mtproto.ErrLangPackInvalid
		log.Errorf("langpack.getLanguages - error: %v", err)
		return nil, err
	}

	_, language, err := service.GetLanguage(ctx, langPack, request.LangCode)
	if err != nil {
		log.Errorf("langpack.getLanguages - error: %v", err)
		err = mtproto.ErrLangCodeNotSupported
		return nil, err
	}

	log.Debugf("langpack.getLanguage - reply %s", language.DebugString())
	return language, nil
}
