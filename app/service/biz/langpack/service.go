// Copyright (c) 2020-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package langpack

import (
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/config"
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/core"
	"github.com/teamgram/teamgram-server/app/service/biz/langpack/dao"
)

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

type Service struct {
	// *core.AuthCore
	*core.LangPackCore
	// *dao.Dao
}

func Init(c config.Config) *Service {
	var (
		s = new(Service)
	)
	//
	//checkErr(paladin.Get("application.toml").UnmarshalTOML(&ac))
	s.LangPackCore = core.New(dao.New(c))

	return s
}

func (service *Service) Close() error {
	return nil
}
