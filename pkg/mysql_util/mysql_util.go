// Copyright (c) 2019-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package mysql_util

import (
	"github.com/teamgram/marmota/pkg/stores/sqlx"
)

var _self *sqlx.DB

func GetSingletonSqlxDB(mysql *sqlx.Config) *sqlx.DB {
	if _self == nil {
		_self = sqlx.NewMySQL(mysql)
	}

	return _self
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}