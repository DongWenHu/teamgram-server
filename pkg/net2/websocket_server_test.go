// Copyright (c) 2019-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package net2

import (
	log "github.com/zeromicro/go-zero/core/logx"
	"testing"
)

type TestPingPongWsServer struct {
	server      *WebsocketServer
	workLoadCnt int
}

func (s *TestPingPongWsServer) Serve() {
	s.server.Serve()
}

func (s *TestPingPongWsServer) Stop() {
	s.server.Stop()
}

func (s *TestPingPongWsServer) isReady() bool {
	return s.server.running
}

func (s *TestPingPongWsServer) OnNewConnection(conn *TcpConnection) {
	log.Info("server OnNewConnection %v", conn.String())
}

func (s *TestPingPongWsServer) OnConnectionDataArrived(conn *TcpConnection, msg interface{}) (err error) {
	log.Info("%s server receive peer(%v) data: %v", s.server.c.ServerName, conn.RemoteAddr(), msg)
	err = conn.Send(msg)
	s.workLoadCnt++
	return err
}

func (s *TestPingPongWsServer) OnConnectionClosed(conn *TcpConnection) {
	log.Info("server OnConnectionClosed - %v", conn.RemoteAddr())
}

func TestWsServer(t *testing.T) {
	protoName := "TestWsProto"
	RegisterProtocol(protoName, &TestWsProto{})

	s := &TestPingPongWsServer{}
	s.server, _ = NewWebsocketServer(&WebsocketConfig{
		Addr:         "0.0.0.0:9999",
		ServerName:   "TestWsServer0",
		ProtoName:    protoName,
		SendChanSize: 1024,
		CertX509:     []string{"cert.pem", "key.pem"},
	}, s)
	s.workLoadCnt = 0

	s.server.Serve()
	select {

	}
}
