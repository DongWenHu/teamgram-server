// Copyright (c) 2019-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package net2

type ClientConfig struct {
	Name      string
	ProtoName string
	AddrList  []string
	EtcdAddrs []string
	Balancer  string
}

type ServerConfig struct {
	Name      string
	ProtoName string
	Addr      string
}
