package websocket

import (
	"encoding/binary"
	"github.com/gorilla/websocket"
	log "github.com/zeromicro/go-zero/core/logx"
	"net"
	"time"
)

// Conn2 represents a WebSocket connection.
type Conn2 struct {
	conn 	*websocket.Conn
	rBuf    []byte
}

func NewConn2(rwc *websocket.Conn) *Conn2 {
	return &Conn2{conn: rwc}
}

func (c *Conn2) Read(p []byte) (n int, err error) {
	for {
		if len(c.rBuf) >= len(p) {
			copy(p, c.rBuf[:len(p)])
			c.rBuf = c.rBuf[len(p):]
			n = len(p)
			break
		}

		var (
			b []byte
			// op int
		)
		if _, b, err = c.conn.ReadMessage(); err != nil {
			return
		} else if len(b) == 0 {
			// break
			continue
		}

		c.rBuf = append(c.rBuf, b...)
	}
	return
}

func (c *Conn2) Write(p []byte) (n int, err error) {
	err = c.conn.WriteMessage(websocket.BinaryMessage, p)
	if err == nil {
		n = len(p)
	}
	return
}

func (c *Conn2) Peek(n int) ([]byte, error) {
	for {
		if len(c.rBuf) >= n {
			return c.rBuf[:n], nil
		}

		if _, b, err := c.conn.ReadMessage(); err != nil {
			return nil, err
		} else {
			c.rBuf = append(c.rBuf, b...)
		}
		log.Debugf("%d", len(c.rBuf))
	}
}

func (c *Conn2) PeekByte() (uint8, error) {
	if b, err := c.Peek(1); err != nil {
		return 0, err
	} else {
		return b[0], nil
	}
}

func (c *Conn2) PeekUint32() (uint32, error) {
	if buf, err := c.Peek(4); err != nil {
		return 0, err
	} else {
		return binary.LittleEndian.Uint32(buf), nil
	}
}

func (c *Conn2) Discard(n int) (int, error) {
	if len(c.rBuf) >= n {
		c.rBuf = c.rBuf[n:]
	}
	return n, nil
}

func (c *Conn2) Close() error {
	return c.conn.Close()
}

func (c *Conn2) LocalAddr() net.Addr {
	return c.conn.LocalAddr()
}

func (c *Conn2) RemoteAddr() net.Addr {
	return c.conn.RemoteAddr()
}

func (c *Conn2) SetDeadline(t time.Time) error {
	return nil
}

func (c *Conn2) SetReadDeadline(t time.Time) error {
	return c.conn.SetReadDeadline(t)
}

func (c *Conn2) SetWriteDeadline(t time.Time) error {
	return c.conn.SetWriteDeadline(t)
}
