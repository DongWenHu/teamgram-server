// Copyright (c) 2019-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package net2

import (
	"sync"
)

type UdpConnection struct {
	addr      string
	codec     Codec
	sendChan  chan interface{}
	sendMutex sync.RWMutex
	isAlive   bool
}

func NewUdpConnection() *UdpConnection {
	return nil
}
