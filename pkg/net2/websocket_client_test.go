package net2

import (
	"crypto/tls"
	"github.com/gorilla/websocket"
	"net/url"
	"testing"
)

func TestWebsocketClient(t *testing.T) {
	conf := &tls.Config{
		InsecureSkipVerify: true,
	}

	dia := &websocket.Dialer{
		TLSClientConfig:   conf,
	}
	u := url.URL{Scheme: "wss", Host: "159.138.151.241:18801", Path: "/apiws"}
	conn, _, err := dia.Dial(u.String(), nil)
	if err != nil {
		t.Log(err)
		return
	}
	defer conn.Close()
	err = conn.WriteMessage(websocket.TextMessage, []byte("hello"))
	if err != nil {
		t.Log(err)
		return
	}
	buf := make([]byte, 100)
	_, buf, err = conn.ReadMessage()
	if err != nil {
		t.Log(err)
		return
	}
	t.Log(string(buf))
}
