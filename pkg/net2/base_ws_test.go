// Copyright (c) 2019-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package net2

import (
	"bufio"
	"io"
	log "github.com/zeromicro/go-zero/core/logx"
	"github.com/teamgram/teamgram-server/pkg/net2/websocket"
)

type TestWsCodec struct {
	*bufio.Reader
	io.Writer
	io.Closer
	mt string

	conn *websocket.Conn
}

func (c *TestWsCodec) Send(msg interface{}) error {
	buf := msg.([]byte)
	if err := c.conn.WriteMessage(websocket.TextMessage, buf); err != nil {
		return err
	}
	c.conn.Flush()
	return nil
}

func (c *TestWsCodec) Receive() (interface{}, error) {
	_, line, err := c.conn.ReadMessage()
	if err != nil {
		return nil, err
	}

	return line, err
}

func (c *TestWsCodec) Close() error {
	return c.Closer.Close()
}

func (c *TestWsCodec) ClearSendChan(ic <-chan interface{}) {
	log.Info("TestWsCodec ClearSendChan, %v", ic)
}

//////////////////////////////////////////////////////////////////////////////////////////
type TestWsProto struct {
}

func (b *TestWsProto) NewCodec(rw io.ReadWriter) (cc Codec, err error) {
	c := &TestWsCodec{
		Reader: bufio.NewReader(rw),
		Writer: rw.(io.Writer),
		Closer: rw.(io.Closer),
		conn:   rw.(*websocket.Conn),
	}
	return c, nil
}
