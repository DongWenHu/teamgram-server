// Copyright (c) 2019-present,  NebulaChat Studio (https://nebula.chat).
//  All rights reserved.
//
// Author: Benqi (wubenqi@gmail.com)
//

package net2

import (
	"fmt"
	"github.com/gorilla/websocket"
	"net/http"
	"runtime/debug"
	"time"

	websocket2 "github.com/teamgram/teamgram-server/pkg/net2/websocket"
	log "github.com/zeromicro/go-zero/core/logx"
)

type WebsocketConfig struct {
	Addr         string
	ServerName   string
	ProtoName    string
	SendChanSize int
	CertX509     []string
}

type WebsocketServer struct {
	c        *WebsocketConfig
	connMgr  *ConnectionManager
	callback TcpConnectionCallback
	running  bool
}

func NewWebsocketServer(c *WebsocketConfig, cb TcpConnectionCallback) (s *WebsocketServer, err error) {
	s = &WebsocketServer{
		c:        c,
		connMgr:  NewConnectionManager(),
		callback: cb,
	}

	return
}

func (s *WebsocketServer) Serve() {
	var err error
	if s.running {
		return
	}
	s.running = true

	http.HandleFunc("/apiws", func(writer http.ResponseWriter, request *http.Request) {
		var upgrader = websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		}
		request.Header.Set("Access-Control-Allow-Origin", "*")
		request.Header.Set("Access-Control-Allow-Credentials", "true")
		request.Header.Set("Access-Control-Allow-Methods", "*")
		request.Header.Set("Access-Control-Allow-Headers", "*")
		conn, err := upgrader.Upgrade(writer, request, nil)
		if err != nil {
			log.Errorf("err: %v", err)
			return
		}
		go s.establishWebsocketConnection(conn)
	})

	go func() {
		if len(s.c.CertX509) != 2 {
			err = http.ListenAndServe(s.c.Addr, nil)
			if err != nil {
				log.Errorf("ListenAndServe: ", err)
				return
			}
		} else {
			err = http.ListenAndServeTLS(s.c.Addr, s.c.CertX509[0], s.c.CertX509[1], nil)
			if err != nil {
				log.Errorf("ListenAndServeTLS: ", err)
				return
			}
		}
	}()
}

func (s *WebsocketServer) Stop() {
	if s.running {
		s.connMgr.Dispose()
		s.running = true
	}
}

func (s *WebsocketServer) Pause() {
}

func (s *WebsocketServer) OnConnectionClosed(conn Connection) {
	s.onConnectionClosed(conn.(*TcpConnection))
}

func (s *WebsocketServer) establishWebsocketConnection(conn *websocket.Conn) {
	conn2 := websocket2.NewConn2(conn)
	codec, _ := NewCodecByName(s.c.ProtoName, conn2)
	tcpConn := NewTcpConnection2(s.c.ServerName, conn2, s.c.SendChanSize, codec, true, s)

	// log.Info("establishTcpConnection...")
	defer func() {
		if err := recover(); err != nil {
			log.Error("tcp_server handle panic: %v\n%s", err, debug.Stack())
			tcpConn.Close()
		}
	}()

	s.onNewConnection(tcpConn)

	for {
		tcpConn.conn.SetReadDeadline(time.Now().Add(time.Minute * 6))
		msg, err := tcpConn.Receive()
		if err != nil {
			log.Error("conn: %s recv error: %v", tcpConn, err)
			return
		}

		if msg == nil {
			// 是否需要关闭？
			log.Error("recv a nil msg by conn: %s", tcpConn)
			continue
		}

		if s.callback != nil {
			// log.Info("onConnectionDataArrived - conn: %s", conn)
			if err := s.callback.OnConnectionDataArrived(tcpConn, msg); err != nil {
				// TODO: 是否需要关闭?
			}
		}
	}
}

func (s *WebsocketServer) onNewConnection(conn *TcpConnection) {
	// log.Info("onNewConnection - conn: %s", conn)
	if s.connMgr != nil {
		s.connMgr.putConnection(conn)
	}

	if s.callback != nil {
		s.callback.OnNewConnection(conn)
	}
}

func (s *WebsocketServer) onConnectionClosed(conn *TcpConnection) {
	// log.Info("onConnectionClosed - conn: %s", conn)
	if s.connMgr != nil {
		s.connMgr.delConnection(conn)
	}

	if s.callback != nil {
		s.callback.OnConnectionClosed(conn)
	}
}

func (s *WebsocketServer) SendByConnID(connID uint64, msg interface{}) error {
	conn := s.connMgr.GetConnection(connID)
	if conn == nil {
		return fmt.Errorf("can not get connection(%d)", connID)
	}
	return conn.Send(msg)
}

func (s *WebsocketServer) GetConnection(connID uint64) *TcpConnection {
	conn := s.connMgr.GetConnection(connID)
	if conn != nil {
		return conn.(*TcpConnection)
	}
	return nil
}
