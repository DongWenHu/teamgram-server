FROM ubuntu:22.04

ARG appname

WORKDIR /app
COPY ${appname} /app/app
COPY ${appname}.yaml /app/config.yaml
COPY config.json /app/config.json

RUN chmod +x /app/app

CMD ["/app/app", "-f=/app/config.yaml"]

